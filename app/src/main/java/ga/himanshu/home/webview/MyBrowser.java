package ga.himanshu.home.webview;

import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Created by Poko on 13-08-2016.
 */
public class MyBrowser extends WebViewClient {
    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        view.loadUrl(url);
        return true;
    }
}
