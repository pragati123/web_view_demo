package ga.himanshu.home.webview;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.Toast;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

public class MainActivity extends AppCompatActivity {
    public WebView mWebView;
    public Button mButton;
    public String cookie;
    public Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mWebView = (WebView) findViewById(R.id.webView);
        context = MainActivity.this;
        mWebView.loadUrl("http://117.55.241.9/student");
        mWebView.setWebViewClient(new MyBrowser());
        mButton = (Button) findViewById(R.id.submit_button);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("Codekamp", "Submit button pressed ");
                cookie = getCookie("http://117.55.241.9/student/LoginServlet", "JSESSIONID");
                Log.d("Codekamp", "Cookie is = " + cookie);


                Ion.with(context)
                        .load("http://117.55.241.9/student/leaveAttendance/consolidatedViewTable.jsp?startDate=25/07/2016&endDate=12/08/2016")
                        .setHeader("Cookie", cookie)
                        .asString()
                        .setCallback(new FutureCallback<String>() {
                            @Override
                            public void onCompleted(Exception e, String result) {

                                Log.d("Codekamp", "RESULT IS =" + result);
                                Toast.makeText(context, result, Toast.LENGTH_LONG).show();
                            }
                        });

            }


        });
    }


    public String getCookie(String siteName, String CookieName) {
        String CookieValue = null;

        CookieManager cookieManager = CookieManager.getInstance();
        String cookies = cookieManager.getCookie(siteName);
        String[] temp = cookies.split(";");
        for (String ar1 : temp) {
            if (ar1.contains(CookieName)) {
                String[] temp1 = ar1.split("=");
                CookieValue = temp1[1];
            }
        }
        return CookieValue;
    }

}
